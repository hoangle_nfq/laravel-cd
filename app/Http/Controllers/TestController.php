<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(Request $request)
    {
        \Log::info(json_encode($request->all()));
        \Log::info($request->all());
        return response()->json([
            'success' => true
        ]);
    }
}
